import React from 'react';

class HatForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '',
            locations: [],
            createdHat: false

        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    handleInputChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }



    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.createdHat;
        delete data.locations;
        console.log(data);

        const HatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(HatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            const cleared = {
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: '',
                createdHat: true
            };
            this.setState(cleared);
        }
    };
    



    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
        };
    }
    
    render() {

        let messageClasses = 'alert alert-success p-2 mt-5 d-none mb-0';
        if (this.state.createdHat) {
        messageClasses = 'alert alert-success mb-0';
        }
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 bg-secondary">
                <h1>Add a new hat to your closet</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.style_name} placeholder="Style_name" required type="text" name="style_name" id="style_name" className="form-control"/>
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="picture_url" className="form-label">Picture Url</label>
                    <textarea onChange={this.handleInputChange} value={this.state.picture_url} required type="text" name="picture_url" id="picture_url" className="form-control" rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleInputChange} value={this.state.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                            return (
                                <option key={location.href} value={location.href}>
                                    Closet: {location.closet_name}, Section: {location.section_number}, Shelf: {location.shelf_number}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-dark">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    Yay! A hat has been added to your closet!
                </div>
            </div>
            </div>
        </div>
        );
    }
}

export default HatForm;